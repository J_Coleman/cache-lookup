//
// Created by jc on 17/5/20.
//
#pragma once

#include "BaseLookup.hpp"
#include <vector>

template <typename KeyType, typename ValueType> class BinaryLookup : public BaseLookup<KeyType, ValueType>
/*
 * Stores key-value pairs in a sorted, resizeable array
 * Lookups to find a key are O(log n)
 * Insertions take O(log n) to find the proper index for the new key-value, however it takes O(n) to move
 * all the elements required. We could eliminate the O(n) by switching to a balanced binary search tree,
 * however that solution has bad cache locality and lookups are much more important than insertions.
 */
{
public:
    BinaryLookup(std::size_t reserve = 1) { _data.reserve(reserve); }

    void insert(const KeyType &key, const ValueType &val)
    {
        // TODO: Binary lookup requires sorted array
        std::size_t insertion_idx = upper_bound(key);
        _data.insert(_data.begin() + insertion_idx, {key, val});
    }

    std::optional<ValueType> lookup(const KeyType &search_key)
    {
        std::ptrdiff_t start = 0;
        std::ptrdiff_t end   = _data.size() - 1;
        while (end >= start)
        {
            std::size_t middle = (start + end) / 2;
            if (search_key > _data[middle].first)
            {
                start = middle + 1;
            } else if (search_key < _data[middle].first)
            {
                end = middle - 1;
            } else if (search_key == _data[middle].first)
            {
                return _data[middle].second;
            }
        }
        return {};
    }

private:
    std::vector<std::pair<KeyType, ValueType>> _data;

    std::size_t upper_bound(const KeyType &search_key)
    {
        std::ptrdiff_t start = 0;
        std::ptrdiff_t end   = _data.size() - 1;
        std::size_t middle;
        while (end >= start)
        {
            middle = (start + end) / 2;
            if (search_key >= _data[middle].first)
            {
                start = middle + 1;
            } else if (search_key < _data[middle].first)
            {
                end = middle - 1;
            }
        }
        return start;
    }
};
