//
// Created by jc on 17/5/20.
//
#pragma once
#include "BaseLookup.hpp"
#include <vector>

template <typename KeyType, typename ValueType> class HashOpenAddressLookup : public BaseLookup<KeyType, ValueType>
/*
 * Open addressing based hashmap. Stores key-value pairs in a resizeable array of buckets.
 * Growth policy with regards to the number of buckets is not considered (size is fixed on construction)
 * To deal with collisions, linear probing is used (increment index by 1).
 */
{
public:
    HashOpenAddressLookup(std::size_t num_elems, double load_factor = 0.5)
    {
        std::size_t num_buckets = num_elems / load_factor;
        num_buckets |= 1; // Make sure its odd

        _buckets.resize(num_buckets, {});
    }

    void insert(const KeyType &key, const ValueType &val)
    /*
     * Doesn't handle inserting the same element multiple times
     * Doesn't handle resizing number of buckets
     *  1. Get the hash index of the bucket
     *  2. If its empty, insert given kev,val pair
     *  3. Else: Increment bucket by 1 and goto step 2.
     */
    {
        std::size_t bucket_idx = generate_hash(key) % _buckets.size();
        for (; bucket_idx < _buckets.size(); bucket_idx = (bucket_idx + 1) % _buckets.size())
        {
            if (!_buckets[bucket_idx])
            {
                _buckets[bucket_idx] = {key, val};
                return;
            }
        }
    }

    std::optional<ValueType> lookup(const KeyType &search_key)
    {
        std::size_t bucket_idx = generate_hash(search_key) % _buckets.size();
        while (_buckets[bucket_idx])
        {
            if (_buckets[bucket_idx]->first == search_key)
            {
                return _buckets[bucket_idx]->second;
            }
            bucket_idx = (bucket_idx + 1) % _buckets.size();
        }
        return {};
    }

private:
    std::size_t generate_hash(const KeyType &key)
    {
        return 39916801 * key; // Ultra basic: multiply by large prime number
    }
    std::vector<std::optional<std::pair<KeyType, ValueType>>> _buckets;
};
