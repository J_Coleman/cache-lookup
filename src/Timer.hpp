//
// Created by jc on 17/5/20.
//
#pragma once
#include <chrono>
#include <iostream>
#include <string>

class Timer
/*
 * Timing Class that increments its internal counter between start() and stop() calls.
 * Can optionally be given a divisor in the constructor:
 *      eg: If you measure 1e3 things, you can set divisor=1e3 to get the Timer
 *          to report based on per thing.
 */
{
public:
    Timer(std::string name, double divisor=1) : _name(name), _repeats(divisor){}
    void start(){
        _start = std::chrono::steady_clock::now();
    }
    void stop(){
        auto end = std::chrono::steady_clock::now();

        // Turns out calls to std::chrono::steady_clock::now() have signficant delay,
        // so we need to eliminate the affect of this overhead in our timing reports.
        auto overhead_start = std::chrono::steady_clock::now();
        auto overhead_stop = std::chrono::steady_clock::now();

        _total += end - _start + overhead_start - overhead_stop;
    }
    ~Timer(){
        std::cout << _name << ": "
                  << std::chrono::duration_cast<std::chrono::nanoseconds>(_total).count()/_repeats
                  << " ns\n";
    }
private:
    std::string _name;
    double _repeats;
    std::chrono::steady_clock::time_point _start;
    std::chrono::duration<double> _total;
};

