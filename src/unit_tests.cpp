//
// Created by jc on 16/5/20.
//
#include "../src/BinaryLookup.hpp"
#include "../src/HashOpenAddressLookup.hpp"
#include "../src/LinearLookup.hpp"
#include "../src/Timer.hpp"
#include "../test/catch.hpp"
#include <bitset>

// MAC addresses are 48bits (but thats not an inbuilt type :( )
struct uint48_t {
    uint48_t() = default;
    uint48_t(uint64_t x) : _x(x) {}
    uint64_t _x : 48; // Tells compiler we only want 48 of the 64 bits
    bool operator==(const uint48_t &other) const { return _x == other._x; }
} __attribute__((packed)); // Tells compiler not to pad struct

using IPv4_t = uint32_t;
// using MAC_t  = std::bitset<48>; // Significantly Slower
using MAC_t = uint48_t;

constexpr int num_runs  = 1e4;
constexpr int num_elems = 1e2;

std::pair<IPv4_t, MAC_t> get_rand_pair()
{
    // Seed with a real random value, if available
    static std::random_device rand_seed;

    std::default_random_engine rand_engine(rand_seed());
    std::uniform_int_distribution<uint32_t> uniform_dist(0, std::numeric_limits<uint32_t>::max());
    return {uniform_dist(rand_engine), uniform_dist(rand_engine)};
}

std::vector<std::pair<IPv4_t, MAC_t>> get_mappings()
{
    std::vector<std::pair<IPv4_t, MAC_t>> mappings;
    mappings.reserve(num_elems);
    for (int j = 0; j < num_elems; ++j)
    {
        mappings.push_back(get_rand_pair());
    }
    return mappings;
}

#define NO_MISSING
#define RUN_LIN_SEARCH_TESTS
#ifdef RUN_LIN_SEARCH_TESTS
TEST_CASE("linear_contains")
{
    Timer logger{"linear_contains", num_runs * num_elems};
    Timer logger2{"linear_insert", num_runs * num_elems};
    using Container = LinearLookup<IPv4_t, MAC_t>;

    std::vector<std::pair<IPv4_t, MAC_t>> mappings = get_mappings();

    for (int i = 0; i < num_runs; ++i)
    {
        Container container{};

        for (const auto &[key, val] : mappings)
        {
            logger2.start();
            container.insert(key, val);
            logger2.stop();
        }

        for (const auto &[key, val] : mappings)
        {
            logger.start();
            auto found_val = container.lookup(key);
            logger.stop();
            REQUIRE(found_val.value() == val);
        }
    }
}

#ifndef NO_MISSING
TEST_CASE("linear_missing")
{
    Timer logger{"linear_missing", num_runs * num_elems};
    std::vector<std::pair<IPv4_t, MAC_t>> mappings;
    mappings.reserve(num_elems);
    for (int j = 0; j < num_elems; ++j)
    {
        mappings.push_back(get_rand_pair());
    }
    using Container = LinearLookup<IPv4_t, MAC_t>;
    for (int i = 0; i < num_runs; ++i)
    {
        Container container{};
        //        std::vector<std::pair<IPv4_t, MAC_t>> mappings = {
        //            {2, 123}, {4, 435}, {5, 345}, {8, 876}, {50, 456}, {51, 91}, {60, 829},
        //        };
        for (const auto &[key, val] : mappings)
        {
            container.insert(key, val);
        }

        for (const auto &[key, val] : mappings)
        {
            // We cheat a little to generate keys that are not in the container
            // Overflow is defined for unsigned types
            logger.start();
            auto found_val = container.lookup(key - 100);
            logger.stop();
            REQUIRE_FALSE(found_val);
        }
    }
}
#endif
#endif

#define RUN_BIN_SEARCH_TESTS
#ifdef RUN_BIN_SEARCH_TESTS

TEST_CASE("binary_contains")
{
    Timer logger{"binary_contains", num_runs * num_elems};
    Timer logger2{"binary_insert", num_runs * num_elems};
    using Container = BinaryLookup<IPv4_t, MAC_t>;

    std::vector<std::pair<IPv4_t, MAC_t>> mappings = get_mappings();

    for (int i = 0; i < num_runs; ++i)
    {
        Container container{};
        // We randomize insert order here, to make sure the insert function is keeping internal data
        // sorted.

        for (const auto &[key, val] : mappings)
        {
            logger2.start();
            container.insert(key, val);
            logger2.stop();
        }
        for (const auto &[key, val] : mappings)
        {
            logger.start();
            auto found_val = container.lookup(key);
            logger.stop();
            REQUIRE(found_val.value() == val);
        }
    }
}

#ifndef NO_MISSING
TEST_CASE("binary_missing")
{
    using Container = BinaryLookup<IPv4_t, MAC_t>;
    Container container{};
    std::vector<std::pair<IPv4_t, MAC_t>> mappings = {{60, 829}, {50, 456}, {4, 435}, {51, 91}, {5, 345}, {8, 876}};
    for (const auto &[key, val] : mappings)
    {
        container.insert(key, val);
    }
    for (const auto &[key, val] : mappings)
    {
        // We cheat a little to generate keys that are not in the container
        // Overflow is defined for unsigned types
        auto found_val = container.lookup(key - 100);
        REQUIRE_FALSE(found_val);
    }
}
#endif
#endif

#define RUN_HASH_SEARCH_TESTS
#ifdef RUN_HASH_SEARCH_TESTS
TEST_CASE("hash_contains")
{
    Timer logger{"hash_contains", num_runs * num_elems};
    Timer logger2{"hash_insertion", num_runs * num_elems};
    using Container = HashOpenAddressLookup<IPv4_t, MAC_t>;

    std::vector<std::pair<IPv4_t, MAC_t>> mappings = get_mappings();

    for (int i = 0; i < num_runs; ++i)
    {
        Container container{mappings.size()};

        for (const auto &[key, val] : mappings)
        {
            logger2.start();
            container.insert(key, val);
            logger2.stop();
        }
        for (const auto &[key, val] : mappings)
        {

            logger.start();
            auto found_val = container.lookup(key);
            logger.stop();
            REQUIRE(found_val.value() == val);
        }
    }
}

#ifndef NO_MISSING
TEST_CASE("hash_missing")
{
    Timer logger{"hash_missing", num_runs * num_elems};
    using Container = HashOpenAddressLookup<IPv4_t, MAC_t>;

    std::vector<std::pair<IPv4_t, MAC_t>> mappings = {
        {2, 123}, {4, 435}, {5, 345}, {8, 876}, {50, 456}, {51, 91}, {60, 829},
    };
    for (int i = 0; i < num_runs; ++i)
    {
        Container container{mappings.size()};

        for (const auto &[key, val] : mappings)
        {
            container.insert(key, val);
        }
        for (const auto &[key, val] : mappings)
        {
            // We cheat a little to generate keys that are not in the container
            // Overflow is defined for unsigned types
            logger.start();
            auto found_val = container.lookup(key - 100);
            logger.stop();
            REQUIRE_FALSE(found_val);
        }
    }
}
#endif
#endif