//
// Created by jc on 17/5/20.
//
#pragma once
#include "BaseLookup.hpp"
#include <vector>

template <typename KeyType, typename ValueType> class LinearLookup : public BaseLookup<KeyType, ValueType>
/*
 * Simply stores given key-value pairs in an unordered, resizeable array
 * Lookups involve O(n) search whilst insertions are *O(1)
 * O(1) average insertion, insertion may involve resizing the array so O(n) worst case.
 */
{
public:
    LinearLookup(std::size_t reserve = 1) { _data.reserve(reserve); }

    void insert(const KeyType &key, const ValueType &val) { _data.emplace_back(key, val); }
    std::optional<ValueType> lookup(const KeyType &search_key)
    {
        for (auto &[key, val] : _data)
        {
            if (search_key == key)
            {
                return val;
            }
        }
        return {};
    }

private:
    std::vector<std::pair<KeyType, ValueType>> _data;
};
