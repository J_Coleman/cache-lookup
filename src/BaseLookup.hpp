//
// Created by jc on 17/5/20.
//
#pragma once

#include <optional>
template <typename KeyType, typename ValueType> class BaseLookup
/*
 * Base Class
 */
{
public:
    BaseLookup() = default;
    virtual void insert(const KeyType &key, const ValueType &val) = 0;
    virtual std::optional<ValueType> lookup(const KeyType &search_key) = 0;
};

